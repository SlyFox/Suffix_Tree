// Суффиксное дерево

#include <fstream>
#include <vector>
#include <map>
#include <boost/algorithm/string/trim.hpp>

struct node{
    node *link;
    int l;
    int r;
    int h;
    std::map<std::string, node*> children;
    node(){
        l = 0;
        r = -1;
        h = 0;
        link = nullptr;
    }
    ~node(){
        for (auto child : children){
            if (child.second != nullptr){
                delete child.second;
                child.second = nullptr;
            }
        }
    }
};

struct state{
    node *active_node;
    node *active_child;
    node *prev_node;
    node *prev_suf; // предыдущий узел, созданный при разбиении ребра надвое (split)
    int pos;
    int cur;
    state(){
        active_node = new node();
        active_child = nullptr;
        prev_node = nullptr;
        prev_suf = nullptr;
        pos = 0;
        cur = 1;
    }
};

void create_node(node *root, state *st, std::string &s, int left = -1, int right = -1){
    node *parent = st->active_node;
    node *new_node = new node();

    if (st->prev_suf != nullptr){
        // создаём суффиксную ссылку
        if (st->active_node == root && st->pos == 0){
            st->prev_suf->link = root;
        }
        else if (st->pos == 0){
            st->prev_suf->link = st->active_node;
        }
        else{
            st->prev_suf->link = new_node;
        }
    }

    if (left == -1){
        new_node->l = st->cur - 1;
    }
    else{
        new_node->l = left;
    }
    if (right != -1){
        new_node->r = right;
    }
    parent->children[s] = new_node;
    st->prev_node = new_node;
}

void split(node *root, std::vector<std::string> &text, state *st, std::string &s){
    std::string s2 = text[st->active_child->l];
    node *prev_child = st->active_child;
    node *parent = st->active_node;
    create_node(root, st, s2, st->active_child->l, st->active_child->l + st->pos);
    prev_child->l = st->active_child->l + st->pos;
    st->active_node = st->prev_node;
    st->active_node->children[text[prev_child->l]] = prev_child;
    st->prev_suf = st->prev_node;
    create_node(root, st, s);
    st->active_node = parent;
    st->active_child = st->prev_suf;
    st->active_child->h = st->active_node->h + st->pos;
}

void suffix_tree(std::vector<std::string> &text, state *st, node *root, int &n){
    std::string s = text.back();
    if (st->pos == 0){
        if (st->active_node->children[s] != nullptr){
            if (st->prev_suf != nullptr){
                st->prev_suf->link = st->active_node;
                st->prev_node = nullptr;
            }
            st->pos++;
            st->prev_node = nullptr;
        }
        else{
            create_node(root, st, s);
        }
        st->prev_suf = nullptr;
        st->active_child = st->active_node->children[s];
    }
    else if (text[st->active_child->l + st->pos] == s){
        st->pos++;
    }
    else{
        split(root, text, st, s);
        n = std::max(st->active_child->h, n);
    }

    if (st->prev_suf != nullptr){
        if (st->active_node != root){
            // переход по суффиксной ссылке
            node *prev_node = st->active_child;
            st->active_node = st->active_node->link;
            st->active_child = st->active_node->children[text[st->active_child->l]];
            // если pos больше, чем расстояние от active_node до active_child
            int d = 0;
            while (st->active_child->r != -1 && st->pos >= st->active_child->r - st->active_child->l){
                st->active_node = st->active_child;
                int d0 = st->active_child->r - st->active_child->l;
                d += d0;
                st->pos -= d0;
                if (st->active_child->children[text[prev_node->l + d]] != nullptr){
                    st->active_child = st->active_node->children[text[prev_node->l + d]];
                }
                else{
                    break;
                }
            }
            prev_node = nullptr;
        }
        else{
            if (root->children[text[st->active_child->l + 1]] != nullptr){
                st->active_child = root->children[text[st->active_child->l + 1]];
                st->pos--;
                node *prev_node = st->active_child;
                // если pos больше, чем расстояние от active_node до active_child
                int d = 0;
                while (st->active_child->r != -1 && st->pos >= st->active_child->r - st->active_child->l){
                    st->active_node = st->active_child;
                    int d0 = st->active_child->r - st->active_child->l;
                    d += d0;
                    st->pos -= d0;
                    if (st->active_child->children[text[prev_node->l + d]] != nullptr){
                        st->active_child = st->active_node->children[text[prev_node->l + d]];
                    }
                    else{
                        break;
                    }
                }
                prev_node = nullptr;
            }
            else{
                st->pos--;
            }
        }
        suffix_tree(text, st, root, n);
    }
    else if (st->prev_node != nullptr && st->active_node != root){
        st->active_node = st->active_node->link;
        suffix_tree(text, st, root, n);
    }
    else if (st->active_child->r != -1 && st->active_child->r - st->active_child->l == st->pos){
        st->pos = 0;
        st->active_node = st->active_child;
    }
}

int solve(std::string filename){
    std::ifstream fin(filename);
    std::vector<std::string> text;
    state *st = new state();
    node *root = st->active_node;
    std::string s;
    int n = 0;
    while(true){
        fin >> s;
        if (!fin){
            break;
        }
        boost::trim(s);
        if (s == ""){
            continue;
        }
        text.push_back(s);
        suffix_tree(text, st, root, n);
        st->cur++;
        st->prev_node = nullptr;
        st->prev_suf = nullptr;
    }
    fin.close();
    delete root;
    delete st;
    return n + 1;
}