#include <gtk/gtk.h>
#include <string>
#include <iostream>
#include "suffix_tree.h"

void find_n_action(GtkWindow *window, GtkWidget **data){
	GtkWidget *label = data[0];
	GtkWidget *label2 = data[1];
	const char *s = gtk_label_get_text(GTK_LABEL(label2));
	std::string filename = std::string(s);
	std::string ans = std::to_string(solve(filename));
	gtk_label_set_text(GTK_LABEL(label), ans.c_str());
}

void file_choice(GtkWindow *window, GtkWidget *label){
	GtkWidget *dialog;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;
	
	dialog = gtk_file_chooser_dialog_new ("Выбор файла",
										  window,
										  action,
										  ("_Отмена"),
										  GTK_RESPONSE_CANCEL,
										  ("_Выбрать"),
										  GTK_RESPONSE_ACCEPT,
										  NULL);
	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT)
	{
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		char *filename = gtk_file_chooser_get_filename (chooser);
		gtk_label_set_text(GTK_LABEL(label), filename);
		g_free (filename);
	}
	
	gtk_widget_destroy (dialog);
}


int main(int argc, char *argv[]){
	// Объявление виджетов:
    GtkWidget *window;
	GtkWidget *button_file_choice;
    GtkWidget *button_find_n;
	GtkWidget *label_file_choice1;
	GtkWidget *label_file_choice2;
	GtkWidget *label_find_n1;
	GtkWidget *label_find_n2;
	GtkWidget *hbox_file_choice, *hbox_find_n;
	GtkWidget *vbox;
		
	gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Вычисление сложности n методом построения суффиксного дерева"); // название окна
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 50);
    gtk_container_set_border_width (GTK_CONTAINER(window), 30);
	
	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
	hbox_file_choice = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
	hbox_find_n = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);
	
	button_find_n = gtk_button_new_with_label("     Найти n      ");
	button_file_choice = gtk_button_new_with_label("Выбрать файл");
	
	std::string str_file_choice1 = "PATH: ";
	std::string str_file_choice2 = "";
	std::string str_find_n1 = "    n = ";
	std::string str_find_n2 = "";
	
	label_file_choice1 = gtk_label_new(str_file_choice1.c_str());
	label_file_choice2 = gtk_label_new(str_file_choice2.c_str());
	label_find_n1 = gtk_label_new(str_find_n1.c_str());
	label_find_n2 = gtk_label_new(str_find_n2.c_str());
	
	gtk_box_pack_start(GTK_BOX(vbox), hbox_file_choice, TRUE, FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox_find_n, TRUE, FALSE, 5);
	
	gtk_box_pack_start(GTK_BOX(hbox_file_choice), button_file_choice, FALSE, FALSE, 3);
	gtk_box_pack_start(GTK_BOX(hbox_file_choice), label_file_choice1, FALSE, FALSE, 3);
	gtk_box_pack_start(GTK_BOX(hbox_file_choice), label_file_choice2, TRUE, TRUE, 3);
	
	gtk_box_pack_start(GTK_BOX(hbox_find_n), button_find_n, FALSE, FALSE, 3);
	gtk_box_pack_start(GTK_BOX(hbox_find_n), label_find_n1, FALSE, FALSE, 3);
	gtk_box_pack_start(GTK_BOX(hbox_find_n), label_find_n2, TRUE, TRUE, 3);
	
	gtk_container_add(GTK_CONTAINER(window), vbox);
	
	gtk_widget_show_all(window);
	
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	
	GtkWidget *labels[] = {label_find_n2, label_file_choice2};
	g_signal_connect(GTK_BUTTON(button_find_n), "clicked", G_CALLBACK(find_n_action), labels);
	g_signal_connect(GTK_BUTTON(button_file_choice), "clicked", G_CALLBACK(file_choice), label_file_choice2);
	
	gtk_main();
	
    return EXIT_SUCCESS;
}
